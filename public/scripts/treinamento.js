//TODO: verificar dependencia de jqueryui. Ficar como node_module?
/**
 * @file Treinamento.js
 * @author Artur Santos
 * @author Gabriel Dias
 * @author getinfo - soluções corporativas
 * @version 0.1
 * @summary Getinfo - Treinamentos front-end
 */

$(document).ready(function () {

    var config = {
        apiKey: "AIzaSyD40L09t-RZmcEXsVDrh3EUfrSFFxHnlWU",
        authDomain: "getinfo-treinamentos.firebaseapp.com",
        databaseURL: "https://getinfo-treinamentos.firebaseio.com",
        projectId: "getinfo-treinamentos",
        storageBucket: "getinfo-treinamentos.appspot.com",
        messagingSenderId: "598472125831"
    };
    firebase.initializeApp(config);

    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            user.getIdToken( /* forceRefresh */ true).then(function (idToken) {
                sessionStorage.setItem('token', idToken);
                loadTrainings();
            }).catch(function (error) {
                alert(error);
            });
        }
    });

    function Question() {
        this.id = null;
        this.description = "";
        this.alternatives = [];
        this.explanation = "";
        this.subject = "";
        this.status = 1;
        this.answer = -1;
        this.reports = [];

        this.addAlternative = function (i, stm) {
            this.alternatives.push({
                index: i,
                statement: stm
            });
        };

        this.delAlternative = function (index) {
            this.alternatives = $.grep(this.alternatives, function (e, i) {
                return e.index != index;
            });
        };

        this.clearAlternatives = function () {
            this.alternatives.length = 0;
        }

        this.addReport = function (report) {
            this.reports.push(report);
        };

        this.delReport = function (id) {
            this.reports = $.grep(this.reports, function (e, i) {
                return e.id != id;
            });
        };

        this.clearReports = function () {
            this.reports.length = 0;
        }

        this.setAnswer = function (index) {
            var found = false;
            for (i = 0; i < this.alternatives.length; i++) {
                if (this.alternatives[i].index == index) {
                    found = true;
                    this.answer = index;
                    break;
                }
            }
            if (!found)
                throw new Error("Índice inválido");
        };
    };

    const newAlternative = '<div><input type="radio" class="rdAlternative" name="rdAlternative" value=""/><textarea rows="2" class="alternative" name="alternative"></textarea><i class="fa fa-trash fa-lg remove_alt_field" aria-hidden="true"></i></div>';
    const newTrainingSubject = '<div><input type="text" class="training-subject" style="width: 90%"><i class="fa fa-trash fa-lg remove_subject_field" aria-hidden="true"></i><br></div>';
    var wrapperTrainingSub = $(".training-subjects");
    var wrapperAlt = $(".alternatives");
    var question = new Question();
    let currentTraining = null;
    var currentRowPosition = null;

    var trainingId = null;
    let imageFile = null;
    var downloadURL = null;
    var editQuestionMode = false;
    
    //exibe mensagem de operação bem sucedida
    function showSuccessModal(successMessage) {
        $(".success-modal-message").text(successMessage);
        $("#success-modal").css("display", "block");
    }

    //exibe mensagem de erro
    function showErrorModal(errorMessage) {
        $(".error-modal-message").text(errorMessage);
        $("#error-modal").css("display", "block");
    }

    //valida um treinamento
    function validateTraining(t) {

        var valid = true;

        if (!t.name) {
            $("#trainingNameErrorMsg").css("display", "inline");
            valid = false;
        } else {
            $("#trainingNameErrorMsg").css("display", "none");
        }

        if (!t.description) {
            $("#trainingDescriptionErrorMsg").css("display", "inline");
            valid = false;
        } else {
            $("#trainingDescriptionErrorMsg").css("display", "none");
        }

        var invalidSubject = false;
        for (i = 0; i < t.subjects.length; i++) {
            if (!t.subjects[i]) {
                invalidSubject = true;
                break;
            }
        }

        if (invalidSubject) {
            $("#trainingSubjectErrorMsg").css("display", "inline");
            valid = false;
        } else {
            $("#trainingSubjectErrorMsg").css("display", "none");
        }

        if (isNaN(t.numberQuestions) || t.numberQuestions < 0) {
            $("#trainingNumQuestionsErrorMsg").css("display", "inline");
            valid = false;
        } else {
            $("#trainingNumQuestionsErrorMsg").css("display", "none");
        }

        if (isNaN(t.duration) || t.duration < 60 * t.numberQuestions) {
            $("#trainingDurationErrorMsg").css("display", "inline");
            valid = false;
        } else {
            $("#trainingDurationErrorMsg").css("display", "none");
        }

        if (isNaN(t.price) || t.price < 0) {
            $("#trainingPriceErrorMsg").css("display", "inline");
            valid = false;
        } else {
            $("#trainingPriceErrorMsg").css("display", "none");
        }

        if (t.status != 0 && t.status != 1) {
            $("#trainingStatusErrorMsg").css("display", "inline");
        } else {
            $("#trainingStatusErrorMsg").css("display", "none");
        }

        return valid;
    }

    //valida uma questão
    function validateQuestion(q) {

        var valid = true;

        if (!q.description) {
            $("#descriptionErrorMsg").css("display", "inline");
            valid = false;
        } else {
            $("#descriptionErrorMsg").css("display", "none");
        }

        if (!q.subject) {
            $("#subjectErrorMsg").css("display", "inline");
            valid = false;
        } else {
            $("#subjectErrorMsg").css("display", "none");
        }

        var invalidAlternative = false;
        for (i = 0; i < q.alternatives.length; i++) {
            if (!q.alternatives[i].statement) {
                invalidAlternative = true;
                break;
            }
        }

        if (invalidAlternative) {
            $("#alternativeErrorMsg").css("display", "inline");
            valid = false;
        } else {
            $("#alternativeErrorMsg").css("display", "none");
        }

        if (q.status != 0 && q.status != 1) {
            $("#statusErrorMsg").css("display", "inline");
        } else {
            $("#statusErrorMsg").css("display", "none");
        }

        return valid;
    }

    //formatter para ícone de edição no tabulator
    var editIcon = function (cell, formatterParams) { //plain text value
        return '<i class="fa fa-pencil-square-o edit-icon" aria-hidden="true"></i>';
    };

    //formatter para valor exibido na coluna de duração do treinamento
    var durationFormatter = function (cell, formatterParams) {
        return parseInt(cell.getValue()) / 60;
    }

    //limpa modal de treinamento
    function clearTrainingModal() {
        $("#trainingName").val('');
        $("#trainingNumQuestions").val('');
        $("#trainingDuration").val('');
        $("#trainingPrice").val('');
        $("#trainingDescription").val('');
        $('#trainingImage').attr('src', "images/blank.png");
        $(".remove_subject_field").map(function () {
            $(this).parent('div').remove();
        });
        var canvas = $("#trainingCanvas")[0];
        canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
    }

    //carrega imagem do treinamento para o canvas
    function loadImage(file) {
        var reader = new FileReader();
        let canvasWidth = $("#trainingCanvas").prop("width");
        let canvasHeight = $("#trainingCanvas").prop("height");
        reader.onload = function (e) {
            imageFile = file;
            var image = new Image();
            image.onload = () => {
                $("#trainingCanvas")[0].getContext("2d").drawImage(image, 0, 0, image.width, image.height, 0, 0, canvasWidth, canvasHeight);
            };
            image.src = window.URL.createObjectURL(file);
        } //end onload
        reader.readAsDataURL(file);
    } //end loadImage

    //dispara ao escolher uma arquivo de imagem
    $("#inputTrainingImage").change(function () {
        var files = this.files;
        if (files && files[0]) {
            loadImage(files[0]);
        }
    });

    $("#selectPhotoBtn").click(function () {
        $('#inputTrainingImage').val("");
        $('#inputTrainingImage').click();
    });

    //carrega dados do treinamento para edição
    var loadTrainingToEdit = function (e, cell) {

        clearTrainingModal();

        //preenchendo com a questão selecionada
        currentTraining = cell.getRow().getData();
        $("#trainingName").val(currentTraining.name);
        $("#trainingNumQuestions").val(currentTraining.numberQuestions);
        $("#trainingDuration").val(parseInt(currentTraining.duration / 60));
        $("#trainingPrice").val(currentTraining.price);
        $("#trainingDescription").val(currentTraining.description);

        var qtdSubjects = currentTraining.subjects.length;
        for (i = 0; i < qtdSubjects; i++) {
            addSubjectField();
        }

        $(".training-subject").each(function (index) {
            $(this).val(currentTraining.subjects[index]);
        });
        
        $("input[name=rdTrainingStatus][value=" + currentTraining.status + "]").prop("checked", true);

        // carregar foto
        if (currentTraining.imageUrl) {
            let img = new Image();
            let canvasWidth = $("#trainingCanvas").prop("width");
            let canvasHeight = $("#trainingCanvas").prop("height");
            img.onload = () => $("#trainingCanvas")[0].getContext("2d").drawImage(img, 0, 0, img.width, img.height, 0, 0, canvasWidth, canvasHeight);
            img.src = currentTraining.imageUrl;
        }
        $("#training-modal").css("display", "block");
    }

    //tabela de treinamentos
    $("#trainings-table").tabulator({
        height: 200, // set height of table, this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
        layout: "fitColumns", //fit columns to width of table (optional)
        resizableColumns: "header",
        columns: [ //Define Table Columns
            {
                formatter: "rownum",
                align: "center",
                width: 30
            },
            {
                formatter: editIcon,
                width: 40,
                align: "center",
                cellClick: loadTrainingToEdit
            },
            {
                title: "Nome",
                field: "name",
                width: 150
            },
            {
                title: "Descrição",
                field: "description",
                align: "left",
                variableHeight: true
            },
            {
                title: "Assuntos",
                field: "subjects",
                width: 100,
                align: "left",
                formatter: "textarea"
            },
            {
                formatter: durationFormatter,
                title: "Duração (min)",
                field: "duration",
                width: 140
            },
            {
                title: "Questões",
                field: "numberQuestions",
                width: 100,
                align: "center"
            },
            {
                title: "Preço",
                field: "price",
                width: 80,
                align: "right",
                formatter: "money"
            },
        ],
        rowClick: function (e, row) {
            trainingId = row.getData().id;
            //console.log(row.getData().includeDate);
            var legenda = "Questões (Treinamento: " + row.getData().name + ")";
            $("#questions-legend").text(legenda);
            $(".tabulator-selectable").css("background-color", "");

            row.getElement().css({
                "background-color": "red"
            });
            loadQuestions(trainingId, $(".rd-filter-questions:checked").val());
        },
    });
    
    //carrega os treinamentos na tabela
    function loadTrainings() {
        let token = sessionStorage.getItem('token');
        let ajaxConfig = {
            headers: {
                "token": token
            }
        }
        $("#trainings-table").tabulator("setData", '/listTrainings', {}, ajaxConfig);
    };

    function getListTrainings() {
        let token = sessionStorage.getItem('token');
        let listTraining = null;
        let ajaxConfig = {
            headers: {
                "token": token
            }
        };

        $.ajax({
            url: '/listTrainings',
            dataType: "json",
            async: false,
            headers: {
                "token": token
            },
            complete: function (data) {
                if (data.status === 200) {
                    listTraining = data.responseJSON;
                }
            }
        });

        return listTraining;
    }

    //limpa modal de questão
    function clearQuestionModal() {
        $(".remove_alt_field").map(function () {
            $(this).parent('div').remove();
        });

        $(".alternative").each(function (index) {
            $(this).val("");
        });

        $("#description").val('');
        $("#subject").val('');
        $("#explanation").val('');
    }

    //carrega dados da questão para edição
    var loadQuestionToEdit = function (e, cell) {
        
        clearQuestionModal();

        //preenchendo com a questão selecionada
        q = cell.getRow().getData();
        $("#description").val(q.description);
        $("#subject").val(q.subject);
        $("#explanation").val(q.explanation);

        var qtdAlternatives = q.alternatives.length;
        for (i = 2; i < qtdAlternatives; i++) {
            addAlternativeField();
        }

        $(".alternative").each(function (index) {
            $(this).val(q.alternatives[index].statement);
        });

        $(".rdAlternative").each(function () {
            if (parseInt($(this).val()) == q.answer) {
                $(this).prop("checked", true);
            }
        });

        $("input[name=rdStatus][value=" + q.status + "]").prop("checked", true);
        $("#question-modal").css("display", "block");
    }

    //tabela de questões
    $("#questions-table").tabulator({
        height: 350, // set height of table, this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
        layout: "fitColumns", //fit columns to width of table (optional)
        resizableColumns: "header",
        columns: [ //Define Table Columns
            {
                formatter: "rownum",
                align: "center",
                width: 40
            },
            {
                formatter: editIcon,
                width: 40,
                align: "center",
                cellClick: loadQuestionToEdit
            },
            {
                title: "Descrição",
                field: "description",
                minWidth: 250,
                align: "left",
                formatter: "textarea"
            },
            {
                title: "Explanação",
                field: "explanation",
                minWidth: 250,
                align: "left",
                formatter: "textarea"
            },
            {
                title: "Assunto",
                field: "subject",
                width: 150,
                align: "left",
                formatter: "textarea"
            },
        ],
        rowClick: function (e, row) {
            question.id = row.getData().id;
        },
    });

    //carrega as questões na tabela
    function loadQuestions(trainingId, status) {
        let token = sessionStorage.getItem('token');
        let ajaxConfig = {
            headers: {
                "token": token
            }
        };

        $.ajax({
            url: `/listQuestions/${trainingId}/${status}`,
            dataType: "json",
            headers: {
                "token": token
            },
            complete: function (data) {
                if (data.status === 200) {
                    $("#questions-table").tabulator("setData", data.responseJSON);
                }
            }
        });
    };

    $(".rd-filter-questions").change(function () {
        loadQuestions(trainingId, $(".rd-filter-questions:checked").val());
    });

    //fecha os modais de treinamento e questão
    $(".close").click(function () {
        $(this).parents().map(function () {
            if ($(this).attr("class") && $(this).attr("class").endsWith("modal"))
                $(this).css("display", "none");
        });
    });

    //insere campos de alternativa no modal de questão
    var addAlternativeField = function () {
        var lastValue = parseInt($(".rdAlternative").last().val());
        $(".alternatives").append(newAlternative);
        $(".rdAlternative").last().val(lastValue + 1);
    }

    $("#addAlternative").click(addAlternativeField);

    //remove campo de alternativa
    $(wrapperAlt).on("click", ".remove_alt_field", function (e) {
        $(this).parent('div').remove();
    });

    var addSubjectField = function () {
        $(".training-subjects").append(newTrainingSubject);
    }

    $("#addTrainingSubject").click(addSubjectField);

    $(wrapperTrainingSub).on("click", ".remove_subject_field", function (e) {
        $(this).parent('div').remove();
    });

    $('#inputQuestionsFile').change(function (e) {
        let file = e.target.files[0];
        let reader = new FileReader();
        let questions = [];
        reader.onload = function (e) {
            let datafile = e.target.result;
            datafile = new Uint8Array(datafile);

            let workbook = XLSX.read(datafile, {
                type: 'array'
            });
            let sheet_name_list = workbook.SheetNames;
            var worksheet = workbook.Sheets[sheet_name_list[0]];
            // indicar os headers de campos não vazios anteriores às alternativas
            var headers = {
                "A": "description",
                "B": "subject",
                "C": "answer"
            };
            var data = [];
            var alt_index = 0;
            const indexOfFirstAlternative = Object.keys(headers).length;
            const lastHeaderKeyBeforeAlternative = Object.keys(headers)[indexOfFirstAlternative - 1]
            console.log(`indexOfFirstAlternative = ${indexOfFirstAlternative}`);
            console.log(`lastHeaderKeyBeforeAlternative = ${lastHeaderKeyBeforeAlternative}`);

            for (z in worksheet) {
                if (z[0] === '!') continue;
                //parse out the column, row, and value
                var col = z.substring(0, 1);
                var row = parseInt(z.substring(1));
                var value = worksheet[z].v;

                //store header names
                if (row == 1) {
                    //headers[col] = value; //ja iniciado com os valores padrões
                    continue;
                }

                if (col === 'A') {
                    alt_index = 0;
                }

                if (!data[row]) data[row] = {};

                if (((headers[col] === undefined) || col >  lastHeaderKeyBeforeAlternative) && value) {
                    headers[col] = `alternative${alt_index}`;
                    alt_index += 1;
                }

                data[row][headers[col]] = value;
            }
            //drop those first two rows which are empty
            data.shift();
            data.shift();
            
            data.forEach((question) => {
                let keys = Object.keys(question);
                
                let alternatives = [];
                keys.forEach((key, index) => {
                    if (key.startsWith('alternative')) {
                        alternatives.push({
                            index: index - indexOfFirstAlternative,
                            statement: question[key]
                        });
                    }
                });
                question.alternatives = alternatives;
                //get training id
                question.trainingId = trainingId;
                question.status = 1;
                try {
                    questions.push(question);
                } catch (e) {
                    error = e;
                }
                alternatives = [];
            }); //end data foreach
            console.log(JSON.stringify(questions));
            $.ajax({
                url: "/question",
                method: "POST",
                dataType: "json",
                data: {
                    questions: questions
                },
                headers: {
                    "token": sessionStorage.getItem('token')
                },
                success: function(){
                    showSuccessModal("Questões salvas com sucesso.");
                },
                error: function (result) {
                    showErrorModal("Erro ao tentar salvar questões!");
                },
                complete: function (data) {
                    loadQuestions(trainingId, $(".rd-filter-questions:checked").val());
                    /* if (data.status === 200)
                        showSuccessModal("Questões salvas com sucesso."); */
                }
            });
        };
        reader.readAsArrayBuffer(file);
    });

    $('#btnQuestionsFile').click(function () {
        $('#inputQuestionsFile').val("");
        $('#inputQuestionsFile').click();
    });

    $('#inputTrainingsFile').change(function(e) {
        let file = e.target.files[0];
        let fileReader = new FileReader();
        fileReader.onload = (e) => {

            var url = e.target.result;
            // Specify the path to the worker
            pdfjsLib.GlobalWorkerOptions.workerSrc = './pdfjs/build/pdf.worker.js';
            /**
             * Retrieves the text of a specif page within a PDF Document obtained through pdf.js 
             * 
             * @param {Integer} pageNum Specifies the number of the page 
             * @param {PDFDocument} PDFDocumentInstance The PDF document obtained 
             **/
            function getPageText(pageNum, PDFDocumentInstance) {
                // Return a Promise that is solved once the text of the page is retrieven
                return new Promise(function (resolve, reject) {
                    PDFDocumentInstance.getPage(pageNum).then(function (pdfPage) {
                        // The main trick to obtain the text of the PDF page, use the getTextContent method
                        pdfPage.getTextContent().then(function (textContent) {
                            var textItems = textContent.items;
                            var finalString = "";

                            // Concatenate the string of the item to the final string
                            for (var i = 0; i < textItems.length; i++) {
                                var item = textItems[i];
                                finalString += item.str;
                            }
                            //console.log(textItems[0]);

                            // Solve promise with the text retrieven from the page
                            resolve(finalString);
                        });
                    });
                });
            }

            pdfjsLib.getDocument(url).then(function(pdf) {
                
                let PDFDocument = pdf;
                let pagesPromises = [];

                for(let i = 0; i < pdf.numPages; i++) {
                    (function (pageNumber) {
                        pagesPromises.push(getPageText(pageNumber, PDFDocument));
                    })(i + 1);
                }

                /*getPageText(1, pdf).then(function(textPage){
                    console.log(textPage);
                })*/

                function makeTraining(name , nQuestions, qTime, status = "1", subjects = [], price = "0", desc) {
                    let convertedPrice = price.replace(',', '.').slice(3);
                    let objStatus = {'Habilitado': 0, 'Desabilitado': 1};
                    return {
                        name: name.trim(),
                        description: desc.trim(),
                        subjects: subjects,
                        duration: parseInt(qTime.trim()) * 60,
                        numberQuestions: parseInt(nQuestions.trim()),
                        price: parseFloat(convertedPrice.trim()),
                        status: objStatus[status.trim()]
                    };
                }

                function splitAndRemountArray(arr, pos, separator) {
                    let aux = arr[pos].split(separator);
                    arr.splice(pos, 1);
                    for(let i = 0; i < aux.length; i++) {
                        arr.push(aux[i]);
                    }
                }

                function generateArrQuestions(arr, arr_gabarito) {
                    let questions = [];
                    arr.shift();
                    arr_gabarito = arr_gabarito.split(/[1-9][0-9]?\)\s?/);
                    arr_gabarito.shift();
                    indexLetter = {
                        "a": 0,
                        "b": 1,
                        "c": 2,
                        "d": 3
                    };

                    for(let i = 0; i < arr.length; i++) {
                        let question = new Question();
                        arr[i] = arr[i].split(/[a-d]\)/);
                        /*arr[i] = arr[i].split('Alternativas');
                        arr[i][1] = arr[i][1].split(/[a-d]\)/);
                        arr[i][1].shift();*/

                        question.description = arr[i][0].trim();
                        for(let j = 1; j < arr[i].length; j++) {
                            question.addAlternative(j - 1, arr[i][j].trim());
                        }
                        
                        question.subject = "ITIL";
                        question.status = 1;
                        question.setAnswer(indexLetter[arr_gabarito[i].trim()]);
                        questions.push(question);
                    }
                    
                    
                    return questions;

                }

                Promise.all(pagesPromises).then(function (pagesText) {
                    
                    let superString = '';
                    for(let i = 0; i < pagesText.length; i++) {
                        superString += pagesText[i];
                    }

                    let arr_root = superString.split(/EXAME|GABARITO/);
                    let arr_training = arr_root[0].split('Número de questões: ');
                    splitAndRemountArray(arr_training, 1, 'Duração (min): ');
                    splitAndRemountArray(arr_training, 2, "Status: ");
                    splitAndRemountArray(arr_training, 3, "Assunto: ");
                    splitAndRemountArray(arr_training, 4, "Preço: ");
                    splitAndRemountArray(arr_training, 5, "Descrição: ");
        
                    let training = makeTraining(arr_training[0], arr_training[1], arr_training[2], arr_training[3], 
                        [arr_training[4].trim()], arr_training[5], arr_training[6]);

                    if(validateTraining(training)) {
                        saveTraining(training);
                    }
                    
                    let listTraining = getListTrainings();
                    trainingId = listTraining[listTraining.length - 1].id;
                    let arr_questions = generateArrQuestions(arr_root[1].split(/[1-9][0-9]?\.\s/), arr_root[2]);
                    arr_questions.forEach((question) => {
                        if(validateQuestion(question)) {
                            saveQuestion(question);
                        }
                    });
                    
                });
                
            }, function(reason) {
                console.log(reason);
            });
        }
        fileReader.readAsArrayBuffer(file);
    });

    $('#btnTrainingsFile').click(function () {
        $('#inputTrainingsFile').val("");
        $('#inputTrainingsFile').click();
    });

    function dataURItoBlob(dataURI) {
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        var byteString = atob(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

        // write the bytes of the string to an ArrayBuffer
        var ab = new ArrayBuffer(byteString.length);

        // create a view into the buffer
        var ia = new Uint8Array(ab);

        // set the bytes of the buffer to the correct values
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        // write the ArrayBuffer to a blob, and you're done
        var blob = new Blob([ab], {
            type: mimeString
        });
        return blob;

    }

    function saveTraining(t) {
        var formData = new FormData();
        //  let canvas = $("#trainingCanvas")[0];

        // let blob = dataURItoBlob(canvas.toDataURL('image/jpeg'));

        formData.append('image', imageFile);
        //formData.append('image',blob);
        formData.append('training', JSON.stringify(t));
        $.ajax({
            url: 'training',
            method: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            async: false,
            headers: {
                "token": sessionStorage.getItem('token')
            },
            complete: function (data) {
                if (data.status === 200) {
                    showSuccessModal("Treinamento salvo com sucesso.");
                    $('#inputTrainingImage').val("");
                    loadTrainings();
                }
            },
            error: (xhr, status, error) => {
                console.error(error);
                showErrorModal("Erro ao tentar salvar treinamento!");
            }
        });
    } //end saveTraining


    $("#saveTraining").click(function (e) {
        e.preventDefault();

        currentTraining.description = $("#trainingDescription").val().trim();
        currentTraining.name = $("#trainingName").val().trim();
        currentTraining.duration = parseInt($("#trainingDuration").val().trim()) * 60;
        currentTraining.price = parseInt($("#trainingPrice").val().trim());
        currentTraining.numberQuestions = parseInt($("#trainingNumQuestions").val().trim());
        currentTraining.subjects = [];
        currentTraining.status = parseInt($(".rdTrainingStatus:checked").val());

        $(".training-subject").map(function () {
            currentTraining.subjects.push($(this).val().trim());
        });

        if (!validateTraining(currentTraining))
            return; //dados de treinamento inválidos.

        saveTraining(currentTraining);
    });

    //insere ou atualiza questão
    function saveQuestion(q) {
        q.trainingId = trainingId;
        var qJSON = JSON.parse(JSON.stringify(q));
        $.ajax({
            url: "/question",
            method: "POST",
            dataType: "json",
            headers: {
                "token": sessionStorage.getItem('token')
            },
            data: {
                questions: [qJSON]
            },
            complete: function (data) {
                if (data.status === 200) {
                    showSuccessModal("Questão salva com sucesso.");
                    loadQuestions(trainingId, $(".rd-filter-questions:checked").val());
                }
            },
            error: function (result) {
                showErrorModal("Erro ao tentar salvar questão!");
            }
        });
    }

    $("#saveQuestion").click(function () {

        question.description = $("#description").val().trim();
        question.subject = $("#subject").val().trim();
        question.explanation = $("#explanation").val().trim();

        //limpando arrays para evitar duplicatas
        question.clearAlternatives();
        question.clearReports();

        $(".alternative").map(function () {
            question.addAlternative($(this).prev(".rdAlternative").val(), $(this).val().trim());
        });

        try {
            question.setAnswer($(".rdAlternative:checked").val());
        } catch (e) {
            $("#answerErrorMsg").css("display", "inline");
            return;
        }

        question.status = $(".rdStatus:checked").val();
        if (validateQuestion(question))
            saveQuestion(question);
    });

    $("#newQuestion").click(function () {
        $(".questionValError").css("display", "none");
        if (!trainingId) {
            showErrorModal("Selecione um treinamento antes de adicionar uma questão.");
            return;
        }
        question.clearAlternatives();
        question.clearReports();

        question = new Question();

        clearQuestionModal();
        $("input[name=rdStatus][value=1]").prop("checked", true);
        $("#question-modal").css("display", "block");
    });

    $("#newTraining").click(function () {
        trainingId = null;
        currentTraining = {};
        clearTrainingModal();
        $("input[name=rdTrainingStatus][value=0]").prop("checked", true);
        $(".trainingValError").css("display", "none");
        $("#training-modal").css("display", "block");
    });

}); //ready