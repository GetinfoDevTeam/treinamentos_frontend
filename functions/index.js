//TODO: veirificar se assunto da questão existe no treinamento
//TODO: mudar os campos state para status
const functions = require('firebase-functions');
const express = require('express');
const admin = require('firebase-admin');
const googleStorage = require('@google-cloud/storage');
const firestore = require('@google-cloud/firestore');
const Busboy = require('busboy');
const fs = require('fs');
let path = require('path');
const getRawBody = require('raw-body');
const contentType = require('content-type');
const bodyParser = require('body-parser');

const firebaseApp = admin.initializeApp(
    functions.config().firebase
);
const db = firebaseApp.firestore();

const storage = googleStorage({
    projectId: "getinfo-treinamentos",
    keyFilename: "./Getinfo Treinamentos-91c9d0192ee8.json"
});

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use((req, res, next) => {
    if (req.rawBody === undefined && req.method === 'POST' && req.headers['content-type'].startsWith('multipart/form-data')) {
        getRawBody(req, {
            length: req.headers['content-length'],
            limit: '10mb',
            encoding: contentType.parse(req).parameters.charset
        }, function (err, string) {
            if (err) return next(err)
            req.rawBody = string
            next()
        })
    } else {
        next()
    }
});

app.use((req, res, next) => {
    if (req.method === 'POST' && req.headers['content-type'].startsWith('multipart/form-data')) {
        const busboy = new Busboy({
            headers: req.headers
        });
        let fileBuffer = new Buffer('');
        req.files = {
            file: []
        }

        busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
            file.on('data', (data) => {
                fileBuffer = Buffer.concat([fileBuffer, data])
            })

            file.on('end', () => { 
                const file_object = {
                    fieldname,
                    'originalname': filename,
                    encoding,
                    mimetype,
                    buffer: fileBuffer
                }

                req.files.file.push(file_object)
                //next()
            })
        });

        busboy.on('field', function (fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
            if (fieldname === 'training') {
                req.body = val;
            }
        });

        busboy.on('finish', function () {
            next();
        });

        busboy.end(req.rawBody);
    } else {
        next()
    }
});

const bucket = storage.bucket("getinfo-treinamentos.appspot.com");

function errorHandler(err, req, res, next) {
    console.error('error handler' + err);
    res.status(500).json({
        status: 500
    });
}

app.use(errorHandler);

app.get('/', (request, response) => {
    response.redirect('/index.html');
});


/* DOC: Início das definições dos tipos para documentação */

/**
 * @typedef Training
 * @property {string} id
 * @property {string} name
 * @property {string} description
 * @property {number} status
 * @property {number} numberQuestions
 * @property {number} duration
 * @property {number} price
 * @property {string[]} subjects
 * @property {Timestamp} includeDate
 * @property {string} [imageUrl]
 */

/**
 * @typedef Alternative
 * @property {number} index
 * @property {string} subject 
 */

/**
 * @typedef Question
 * @property {string} trainingId
 * @property {Alternative[]} alternatives
 * @property {string} description
 * @property {string} [explanation]
 * @property {number} status
 * @property {string} subject
 */

 /* Fim das definições de tipos para documentação */

 // Retorna uma lista de treinamentos
app.get('/listTrainings', (request, response) => {
    let tokenId = request.get('token');
    var trainings = [];
    admin.auth().verifyIdToken(tokenId).then((decodedToken) => { //usuário autenticado

        db.collection("trainings").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                trainings.push(doc.data());
                trainings.sort((dateA, dateB) => {
                    return dateA.includeDate - dateB.includeDate;
                });
            });
        }).then(function () { // lista preenchida com todos os treinamentos
            response.json(trainings);
        });
    }).catch((error) => {
        response.json(trainings);
    });
});

// Retorna lista de questões do treinamento de id=trainingId e status = status
app.get("/listQuestions/:trainingId/:status", (request, response) => {
    let tokenId = request.get('token');
    let trainingId = request.params.trainingId;
    let status = parseInt(request.params.status);
    var questions = [];

    if (!trainingId)
        response.json(questions);

    try {
        admin.auth().verifyIdToken(tokenId).then((decodedToken) => {
            db.collection("trainings").doc(trainingId).collection("questions").get().then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    switch (status) {
                        case -1: //adiciona todas a questões do treinamento, independente do field status
                            questions.push(doc.data());
                            break;
                        default:
                            if (doc.data().status === status)
                                questions.push(doc.data());
                            break;
                    }
                });
                response.json(questions);
            });
            
        }).catch(error => {
            response.json(questions);
            console.error(`listQuestions ${trainingId} -  ${status} - ${e}`);
        });
    } catch (e) {
        response.json(questions);
        console.error(`listQuestions ${trainingId} -  ${status} - ${e}`);
    };
});

/**
 *  Valida uma questão e retorna uma ista com zero ou mais erros
 * @param {Question} q - Questão
 * @returns {[string]} 
 */
const validateQuestion = (q) => {
    let erros = [];

    if (!q.trainingId) {
        erros.push('id de treinamento inválido');
    }

    if (!q.alternatives || q.alternatives.length == 0) {
        erros.push('alternativas inválidas');
    }

    let found = false;
    q.alternatives.forEach((alt, index) => {
        if (alt.index === q.answer) {
            found = true;
        }
        if (!alt.statement)
            erros.push(`index ${index} : statement inválido`);
    });

    if (!found) {
        erros.push('indice de resposta inválido!');
    }

    if (!q.description) {
        erros.push('descrição inválida');
    }

    if (![0, 1].includes(q.status)) {
        erros.push('estado inválido');
    }

    if (!q.subject) {
        erros.push('assunto inválido');
    }

    return erros;
}

/**
 * Salva uma questão no firestore
 * @param {Question} q
 * @throws {InvalidArgumentException} Dispara erro caso a questão possua dados inválidos
 */
const saveQuestion = (q) => {

    q.status = parseInt(q.status);
    q.answer = parseInt(q.answer);
    q.alternatives.forEach((alt, index) => {
        alt.index = parseInt(alt.index);
    });

    if (!q.reports) {
        q.reports = [];
    }

    if(!q.explanation){
        q.explanation = "";
    }

    let errors = validateQuestion(q);
    let questionRef = null;
    if (errors.length !== 0)
        throw errors;

    
    if (q.id) {
        questionRef = db.collection("trainings").doc(q.trainingId).collection("questions").doc(q.id);
    } else {
        questionRef = db.collection("trainings").doc(q.trainingId).collection("questions").doc();
    }

    let prom = new Promise((resolve, reject) => {

        questionRef.set({
            alternatives: q.alternatives,
            answer: q.answer,
            description: q.description,
            explanation: q.explanation,
            id: questionRef.id,
            reports: q.reports,
            status: q.status,
            subject: q.subject,
        }).then(() => { //questão salva
            resolve('success');
        }).catch((err) => { //erro ao tentar salvar a questão
            reject(err);
        });
    });

    return prom;
}

/**
 * Recebe uma lista de questões e chama saveQuestion() para cada uma
 * @param {[Question]} questions
 */

const saveQuestions = (questions) => {

    let savedQuestions = Array(questions.length).fill(false);
    let prom = new Promise((resolve, reject) => {

        questions.forEach((q, index) => {
            saveQuestion(q)
                .then(() => {
                    savedQuestions[index] = true;
                    if (savedQuestions.every(value => value === true)) //se todas as questões foram salvas
                        resolve('success');
                })
                .catch((err) => {
                    if (err) {
                        reject(err);
                    }
                });
        }); //emd foreach
    });

    return prom;
}

// Salva as questão recebidas em req.body
app.post("/question", (req, res) => {
    let tokenId = req.get('token');

    admin.auth().verifyIdToken(tokenId).then((decodedToken) => { //usuário autenticado
        console.log(`question autenticado`);
        saveQuestions(req.body.questions)
            .then(() => { //questões salvas
                res.status(200).json({
                    status: 200
                });
            })
            .catch((err) => {
                res.status(500).json({
                    status: 500
                });
            })
    }).catch(error => {
        res.status(500).json({
            status: 500
        });
    });
});

/**
 * Salva a imagem de um treinamento no firebase storage
 * @param {string} folder_id - id da pasta (id do treinamento)
 * @param {Buffer} content - buffer com conteúdo do arquivo
 * @param {string} extension - extensão do arquivo 
 */
const uploadImageToStorage = (folder_id, content, extension) => {
    let prom = new Promise((resolve, reject) => {

        let filenameInStorage = 'image' + extension;
        let fileUpload = bucket.file('treinamentos/' + folder_id + '/' + filenameInStorage);

        fileUpload.save(content, (err) => {
            if (err) {
                console.error(`inside upload: file=${file_cropped} erro=${err}`);
                reject(err);
            }
            let downloadURL = `https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/treinamentos%2F${folder_id}%2F${filenameInStorage}?alt=media`;
            resolve(downloadURL); //resolve com a url da imagem
        });
    });
    return prom;
}


/**
 * Valida um treinamento e retorna uma lista com zero o mais erros
 * @param {Training} t - Treinamento
 * @returns {[string]}
 */
const validateTraining = (t) => {
    let errors = [];
    
    if (!t.id) {
        errors.push('id inválido');
    }

    if (!t.name) {
        errors.push('nome inválido');
    }

    if (!t.description) {
        errors.push('descrição inválida');
    }

    if (isNaN(t.numberQuestions) || t.numberQuestions < 1) {
        errors.push('número de questões inválido');
    }

    if (isNaN(t.duration) || (t.duration < 60 * t.numberQuestions)) {
        errors.push('duração inválida');
    }

    if (isNaN(t.price) || t.price < 0) {
        errors.push('preço inválido');
    }

    /*if (!t.subjects || t.subjects.length === 0) {
        errors.push('assuntos inválidos');
    }*/

    if(![0,1].includes(t.status)){
        errors.push(`estado inválido:${t.status}`);
    }
   
    return errors;
}


/**
 * Salva um treinamento no firestore
 * @param {Training} t - Treinamento
 * @param {FirebaseFirestore.DocumentReference} trainingRef - Referência ao novo treinamento contendo o novo id
 */
const saveTraining = (t, trainingRef) => {

    let prom = new Promise((resolve, reject) => {
        if (!t.subjects) {
            t.subjects = [];
        }
    
        t.duration = parseInt(t.duration);
        t.price = parseInt(t.price);
        t.numberQuestions = parseInt(t.numberQuestions);
        
        if(!t.includeDate) {
            let today = new Date();
            t.includeDate = today;
        }

        let newTraining = {
            description: t.description,
            duration: t.duration,
            id: trainingRef.id,
            name: t.name,
            numberQuestions: t.numberQuestions,
            price: t.price,
            subjects: t.subjects,
            includeDate: t.includeDate,
            status: t.status
        };

        if (t.imageUrl) {
            newTraining.imageUrl = t.imageUrl;
        }

        let errors = validateTraining(t);

        if (errors.length === 0) {
            trainingRef.set(newTraining)
            .then(() => {
                console.log(`saved id=${t.id}`);
                resolve('success');
            })
            .catch((err) => {
                reject(err);
            })
        } else {
            console.error(`saveTraining func: t=${JSON.stringify(t)} erro=${errors}`);
            reject(errors);
        }
    });

    return prom;
}

// Salva um treinamento
app.post("/training", (req, res) => {

    try {

        console.log('post training');
        let tokenId = req.get('token');
        let busboy = new Busboy({
            headers: req.headers
        });
        console.log('post training new busboy');
        admin.auth().verifyIdToken(tokenId).then((decodedToken) => {

            console.log('post training autenticado');
            console.log(`req.body=${req.body}`);
            let t = JSON.parse(req.body);
            let trainingRef = null;
            if (t.id) { //edit
                trainingRef = db.collection("trainings").doc(t.id);
            } else { //insert
                trainingRef = db.collection("trainings").doc();
                t.id = trainingRef.id;
                t.status = 0;
            }

            let file = req.files.file[0];

            if (file) {
                console.log('file not null...');

                let imageExtension = path.extname(file.originalname);
                console.log(imageExtension);

                uploadImageToStorage(trainingRef.id, file.buffer, imageExtension).then((downloadURL) => {
                    console.log('post training inside upload then');
                    t.imageUrl = downloadURL;
                    saveTraining(t, trainingRef)
                        .then(() => {
                            res.status(200).json({
                                status: 200
                            });
                        })
                        .catch(err => {
                            console.log(`erro saveTraining com upload de imagem. erro${err}`);
                            if (err) {
                                throw err;
                            }
                        })
                    
                }).catch((err) => { //error uploadImage
                    console.error(`catch upload: t.id=${t.id} imgURL=${t.imageUrl} erro=${err}`);
                    
                    if (err) {
                        throw err;
                    }
                });

            } else { // file é nulo
                console.log('sem arquivo');
                try {
                    saveTraining(t, trainingRef)
                        .then(() => {
                            res.status(200).json({
                                status: 200
                            });
                        })
                        .catch(err => {
                            console.log(`erro saveTraining sem upload de imagem. erro${err}`);
                            if (err) {
                                throw err;
                            }
                        });
                } catch (err) {
                    console.error(`catch else saveT: t.id=${t.id} erro=${err}`);
                    if (err) {
                        throw err;
                    }
                }
            } //end if file
        }).catch(err => {
            console.error(`catch verifytoken: token=${tokenId} erro=${err}`);
            if (err) {
                res.status(500).json({
                    status: 500
                });
            }
        });
    } catch (err) {
        console.error(`catch final: erro=${err}`);
        res.status(500).json({
            status: 500
        });
    }
}); //end post

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.app = functions.https.onRequest(app);